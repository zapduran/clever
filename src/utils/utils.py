from enum import Enum

from selenium.webdriver.common.by import By


class MonthEnum(Enum):
    JAN = 1
    FEB = 2
    MAR = 3
    APR = 4
    MAY = 5
    JUN = 6
    JUL = 7
    AUG = 8
    SEP = 9
    OCT = 10
    NOV = 11
    DEC = 12


def get_month_element(month):
    return By.CSS_SELECTOR, f'span.month:nth-child({month})'
