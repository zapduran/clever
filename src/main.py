import logging
import os
from time import sleep
from dotenv import load_dotenv

import boto3
from botocore.exceptions import ClientError
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

from src.pages.consult_page import ConsultPage
from src.pages.home_page import HomePage
from src.utils import utils


def upload_file(file_name, bucket_name, object_name=None):
    if object_name is None:
        object_name = os.path.basename(file_name)

    s3_client = boto3.client('s3')

    try:
        logging.info(f'sending object {object_name} to s3')

        s3_client.upload_file(file_name, bucket_name, object_name)
    except ClientError as e:
        logging.error(e)
        return False
    return True


def main(driver):
    driver.get(os.getenv('URL'))

    home = HomePage(driver)
    consult = ConsultPage(driver)

    # navigate
    home.click_benefits_citizen()
    sleep(1)
    home.click_consult()

    sleep(1)
    # filter
    consult.open_period_filters()
    sleep(1)
    consult.open_initial_date()
    sleep(1)
    consult.click_prev_year_button()
    sleep(1)
    consult.click_month_button(utils.MonthEnum.JAN)
    sleep(1)
    consult.open_finish_date()
    sleep(1)
    consult.click_prev_year_button()
    sleep(1)
    consult.click_month_button(utils.MonthEnum.DEC)
    sleep(1)
    consult.click_add_filters_btn()

    sleep(1)
    # search
    consult.click_search_results()

    sleep(1)
    # download
    consult.click_download_button()

    if upload_file(f'{os.getcwd()}/data/beneficio.csv', os.getenv('BUCKET')):
        logging.info('Sent Object')

    sleep(3)
    driver.quit()


if __name__ == "__main__":
    load_dotenv()

    logging.basicConfig(level=logging.INFO)
    logging.info('Init Application')

    options = Options()
    prefs = {"download.default_directory": fr'{os.getcwd()}/data'}
    options.add_experimental_option("prefs", prefs)

    webdriver = webdriver.Chrome(options=options)

    main(webdriver)
