from src.pages.base_page import BasePage
from src.resources.locators import ConsultPageLocators
from src.utils import utils


class ConsultPage(BasePage):
    def __init__(self, driver):
        super().__init__(driver)
        self.driver = driver

    def open_period_filters(self):
        self.click(ConsultPageLocators.period_btn)

    def open_initial_date(self):
        self.enter_text(ConsultPageLocators.initial_date_input, '')

    def open_finish_date(self):
        self.enter_text(ConsultPageLocators.finish_date_input, '')

    def click_prev_year_button(self):
        self.execute_script(ConsultPageLocators.prev_date_button)

    def click_month_button(self, month):
        match month:
            case utils.MonthEnum.JAN:
                self.click(ConsultPageLocators.jan_month_button)
            case utils.MonthEnum.FEB:
                self.click(ConsultPageLocators.jan_month_button)
            case utils.MonthEnum.DEC:
                self.click(ConsultPageLocators.dec_month_button)

    def click_add_filters_btn(self):
        self.execute_script(ConsultPageLocators.add_filters_btn)

    def click_search_results(self):
        self.click(ConsultPageLocators.search_results_btn)

    def click_download_button(self):
        self.execute_script(ConsultPageLocators.download_btn)
