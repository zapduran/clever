class BasePage:
    def __init__(self, driver):
        self.driver = driver

    def click(self, by_locator):
        self.driver.find_element(*by_locator).click()

    def execute_script(self, by_locator):
        self.driver.execute_script("arguments[0].click();", self.driver.find_element(*by_locator))

    def enter_text(self, by_locator, text):
        self.driver.find_element(*by_locator).send_keys(text)

    def scroll_down(self):
        self.driver.execute_script("window.scrollTo(0,document.body.scrollHeight)")

    def find_element(self, locator):
        return self.driver.find_element(*locator)

    def find_elements(self, locator):
        return self.driver.find_elements(*locator)
