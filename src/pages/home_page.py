from selenium.common import NoSuchElementException

from src.pages.base_page import BasePage
from src.resources.locators import HomePageLocators


class HomePage(BasePage):
    def __init__(self, driver):
        super().__init__(driver)
        self.driver = driver

    def click_benefits_citizen(self):
        self.click(HomePageLocators.benefit_btn)

    def click_consult(self):
        self.click(HomePageLocators.consult_btn)
