from selenium.webdriver.common.by import By

from src.utils import utils


class HomePageLocators:
    benefit_btn = By.ID, 'beneficios-ao-cidadao-card'
    consult_btn = By.CSS_SELECTOR, '#beneficios-ao-cidadao-links li:last-child a'

    filter_li = By.CSS_SELECTOR, '#id-box-filtro .box-tabela-filtro__lista-botoes'
    period_btn = By.CSS_SELECTOR, 'li:nth-child(2) button'


class ConsultPageLocators:
    filter_li = By.CSS_SELECTOR, '#id-box-filtro .box-tabela-filtro__lista-botoes'
    period_btn = By.CSS_SELECTOR, 'li:nth-child(2) button'

    initial_date_input = By.ID, 'de'
    finish_date_input = By.ID, 'ate'
    prev_date_button = By.CLASS_NAME, 'prev'
    next_date_button = By.CLASS_NAME, 'next'
    jan_month_button = utils.get_month_element(1)
    feb_month_button = utils.get_month_element(2)
    dec_month_button = utils.get_month_element(12)
    add_filters_btn = By.CSS_SELECTOR, 'input[type="button"]'
    search_results_btn = By.CSS_SELECTOR, 'button.btn-consultar'

    download_btn = By.CSS_SELECTOR, 'a#btnBaixar'
